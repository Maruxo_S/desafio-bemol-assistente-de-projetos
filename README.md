**# Desafio Bemol - Assistente de Projetos**

Relatório Técnico do Sistema

#REQUISITOS PARA FUNCIONAMENTO DO WINDOWS# 

# Configuração de ambiente Windows 10 x64bits
- Instalar Python versão 3.8.5 
  Link para Download: https://www.python.org/downloads/release/python-385/

#Bibliotecas
- Panda versão 1.1.3 (https://pypi.org/project/pandas/)
- Matplotib versão 3.3.2 (https://pypi.org/project/matplotlib/)
- Datetime



#REQUISITOS PARA FUNCIONAMENTO NO LINUX#

# Configuração de ambiente Linux 20.04
- Instalar Python versão 3.8.5 
  Link para Download: https://www.python.org/downloads/release/python-385/

#Bibliotecas
- Panda versão 1.1.3 (https://pypi.org/project/pandas/)
- Matplotib versão 3.3.2 (https://pypi.org/project/matplotlib/)
- Datetime

#others configures



