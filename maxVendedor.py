import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from pandas import DataFrame 
from datetime import date

def getSalesPerson():
    excel_file = './db/dataset.xlsx'
    data = pd.read_excel(excel_file)    
    sales = data.groupby('usuario', as_index = False).agg({'valor_compra': 'sum', 'nome': 'unique'})
    limit= len(sales)
    maximuns = sales.nlargest(15, ['valor_compra'])
    return maximuns

def table():
    title_text = 'Relatorio: Ranking de Valores de Compra'
    footer_text = pd.to_datetime(date.today(), format='%Y %m %d').strftime('%d/%m/%Y')
    background_color = 'skyblue'
    border = 'steelblue' 

    data = getSalesPerson()
    
    Data = []
    
    column_headers = [
        'Valor total de compras',
        'Vendedor', 
    ]

    keys = data['valor_compra'].keys()
    for i in keys:
        Data.append([
            data['valor_compra'][i],
            data['nome'][i],
        ])
    
    row_headers = [x for x in Data]
    cell_text = []

    for row in Data:
        cell_text.append([x for x in row])

    rcolors = plt.cm.BuPu(np.full(len(row_headers), 0.1))
    ccolors = plt.cm.BuPu(np.full(len(column_headers), 0.1))
    
    plt.figure(linewidth=2,
            edgecolor=border,
            facecolor=background_color,
            tight_layout={'pad':1},
            )
    the_table = plt.table(cellText=cell_text,
                        colColours=ccolors,
                        colLabels=column_headers,
                        loc='center')
    
    the_table.scale(1, 1.5)
    
    ax = plt.gca()
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    
    plt.box(on=None)
    
    plt.suptitle(title_text)
    
    plt.figtext(0.95, 0.05, footer_text, horizontalalignment='right', size=6, weight='light')

def graph():
    maximuns = getSalesPerson()
    df = DataFrame(maximuns, columns=['nome', 'valor_compra'])
    ax = df.plot(x = 'nome', y = 'valor_compra', kind = 'bar')
    for p in ax.patches:
        ax.annotate(str(round(p.get_height(), 2)), (p.get_x() * 1.005, p.get_height() * 1.005))

def main():
    graph()
    table()
    plt.show()

if __name__ == '__main__':
    main()